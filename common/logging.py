import os
import logging
from dotenv import load_dotenv, find_dotenv

def setup_logging():
    _ = load_dotenv(find_dotenv()) # read local .env file

    log_level = os.environ.get('LOG_LEVEL', 'WARNING').upper()
    logging.basicConfig(level=getattr(logging, log_level, logging.WARNING), format='%(asctime)s - %(levelname)s - %(message)s')
