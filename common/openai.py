import os
import openai
from common.const import const

def setup_openai():
    openai.api_key = os.getenv('OPENAI_API_KEY')

    if openai.api_key:
        print(f'Using API Key: {openai.api_key[:5]}***')
    else:
        print('API Key not found.')
        
def get_completion(
    prompt, model=const.MODEL
):
    messages = [{"role": "user", "content": prompt}]
    response = openai.ChatCompletion.create(
        model=model,
        messages=messages,
        temperature=0,  # this is the degree of randomness of the model's output
    )
    return response.choices[0].message["content"]