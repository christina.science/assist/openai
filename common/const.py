import tiktoken

class Constants:
    @property
    def MODEL(self):
        return 'gpt-4'
    @property
    def ENCODING(self):
        return tiktoken.encoding_for_model(self.MODEL)
    @property
    def PRICE_PER_PROMPT_TOKEN(self):
        return 0.03 / 1000
    @property
    def PRICE_PER_COMPLETION_TOKEN(self):
        return 0.06 / 1000
    @property
    def MAX_PRICE_PER_PROMPT(self):
        return 0.01

const = Constants()
