from common.const import const

def tokenify(string: str, encoding: str=const.ENCODING) -> int:
    """Returns the number of tokens in a text string."""
    num_tokens = len(encoding.encode(string))
    
    return num_tokens, f"${const.PRICE_PER_PROMPT_TOKEN * num_tokens:.4f}"