# OpenAI Playground

## Setup

Create a local `.env` file, copy `.env.template`, and fill with your details

```bash
cp .env.template .env
```

If you don't have a virtual environment set up, create one using the following commands:

```bash
python -m venv venv
```

Activate the virtual environment

```bash
source venv/bin/activate
```

Install dependencies

```bash
pip install --trusted-host pypi.python.org -r requirements.txt
pip-compile requirements.in -o requirements.txt --upgrade --resolver=backtracking
```